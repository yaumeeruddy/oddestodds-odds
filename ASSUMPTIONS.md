# ODDESTODDS ODDS

## ASSUMPTIONS

1. Any user is considered a Punter until logged in as an odds handler. Not into gambling or the sort myself, I see it the way an internet user surf online on his/her web browser without having to log in and viewing the info.

2. An odds handler can view & modify all odds in the application, even if not the creator. An odds would probably not be duplicated in the system. So if there is a need to manage an existing particular odds, another odds handler can do it as well.

3. Since this is a console application, it does not really help much in visual or navigation aspects. The application has been coded so that entering a dot symbol (.) in input fields can be used to go back to the previous screen or cancelling input for that field, depending on the current screen.

Adding on that last item, I think making the application into an ASP.NET MVC one would have been much better. It would have helped a lot in UI terms, which is different in a console application.  

Since I already spent quite a number of hours with the console application from the start, I decided to continue with it.