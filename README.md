# ODDESTODDS ODDS

Description: .NET Core 2.2 Console application to display odds in real time, as well as managing them.

## USERS

There are two types of user using this application:

**1. Punter**

- Currently not tracked in the application.
- Any user is considered a punter until logged in as an odds handler.
- The punter can view all published active odds in real-time on the first main screen.

**2. Odds Handler**

- Tracked in the application (table OddsHandlers / entity OddsHandler)
- Has a username & password.
- Similar to an administrator, an odds handler can manage(create/update/delete/view) all odds in the system.
- For quick setup purposes, a dummy odds handler record is added at startup if none exists (with data from the config file appsettings.json).

## ODDS

An odds has the following properties in the application:  

- **Id** : Unique identifier in database
- **Name** : Name of odds, to know what the odds refers to.
- **Description** : More details about the odds, if needed.
- **N1** : The value for the odds titled '1'. E.g. represents the odds for a Home Win in football.
- **NX** : The value for the odds titled 'X'. E.g. represents the odds for a Draw in football.
- **N2** : The value for the odds titled '2'. E.g. represents the odds for a Away Win in football.
- **OddsHandlerId** : Id of the Odds handler who created or last updated the odds.
- **Published** : States whether the odds can be made public i.e. shown to punters.
- **CreatedDate** : Datetime when the odds was created.
- **LastUpdateDate** : Datetime when the odds was last updated.
- **EndDate** : Datetime until when the odds is valid.

## appsettings.json

This is a configuration file that contains the database connection string, which should be changed prior to the database updates. [See Migrations in EF Core](https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/)   
As mentioned earlier in the **USERS** section, a dummy odds handler record can also be added at startup if none exists, given the configurations
"DummyOddsHandler -> Username" and "DummyOddsHandler -> Password" exist.

~~~~
{
  "ConnectionStrings": {
    "Odds": "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Odds;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"
  },
  "DummyOddsHandler": {
    "Username": "JohnDoe",
    "Password": "Abc*123"
  }
}
~~~~

## SqlTableDependency

The application makes use of the [SqlTableDependency component](https://www.nuget.org/packages/SqlTableDependency/), to listen to database changes notifications, hence the real-time part.  It requires the service broker to be enabled on the database. This can be achieved by running the following SQL query (replace DBNAME by the database name):

~~~~
ALTER DATABASE [DBNAME] SET ENABLE_BROKER WITH ROLLBACK IMMEDIATE
~~~~

## Starting the application

### MAIN SCREEN

When the application is started, it should look like below.

![Main screen image](images/capture1.png)

1. This is a list of published active odds that is updated in real-time, everytime there is a change (Create/Update/Delete) in the Odds database table.
2. Represents a menu, where the arrow keys are used to select the menu item (with the ENTER key press). As an Odds Handler, the first menu item "Login as Odds Handler" should be selected, to login obviously and be able to manage the odds.

### LOGIN SCREEN FOR ODDS HANDLER

![Login screen image](images/capture2.png)

- The application will request the user to provide his/her username and password, both are required.
- The password characters are hidden on-screen.
- If the login credentials provided are not good, another attempt can be made after the confirmation message.
- Entering a dot symbol(.) in any of the two input fields will switch back to previous screen.

### MAIN SCREEN FOR ODDS HANDLER

![Login screen image](images/capture3.png)

- In this screen, the upper part is a list of all odds and the lower part is a menu of actions that can be made.
- The upper part is also a 2nd level menu if the menu item "Update odds" or "Delete odds" (from the lower part) is selected.
- If the "Create odds" menu item is selected, a new screen will be displayed where the odds handler can enter data for a new odds.
- If the "Update odds" menu item is selected, the odds handler will first need to select the odds (from list in upper part) that he/she wishes to update. After entering that selection, a new screen will be displayed where the odds handler can update data for the selected odds.
- If the "Delete odds" menu item is selected, the odds handler will first need to select the odds (from list in upper part) that he/she wishes to delete. After entering that selection, the user will be prompted with a confirmation message to proceed with the delete.
- If the "View published odds" menu item is selected, a new screen will be displayed showing a list of published active odds. This will be similar to the list of odds in the main screen, however it is not updated in real-time.