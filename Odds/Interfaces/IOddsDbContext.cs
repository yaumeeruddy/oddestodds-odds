﻿using Microsoft.EntityFrameworkCore;
using Odds.Entities;

namespace Odds.Interfaces
{
    public interface IOddsDbContext
    {
        DbSet<OddsHandler> OddsHandlers { get; set; }
        DbSet<Odd> Odds { get; set; }

        int SaveChanges();
    }
}
