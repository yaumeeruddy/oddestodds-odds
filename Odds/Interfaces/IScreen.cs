﻿namespace Odds.Interfaces
{
    public interface IScreen
    {
        void Draw();
    }
}
