﻿using Odds.CRUD;
using Odds.Screens;

namespace Odds
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ServiceManager.Init();
            OddsHandlerManagement.AddDummyUserIfEmpty();
            new MainMenuScreen().Draw();
        }
    }
}
