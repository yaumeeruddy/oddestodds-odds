﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace Odds
{
    public class ConfigurationManager
    {
        private static IConfiguration Configuration;

        public static string GetValue(string key)
        {
            if(Configuration == null)
            {
                var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

                Configuration = builder.Build();
            }
            return Configuration[key];
        }

        public static string GetConnectionString()
        {
            return GetValue("ConnectionStrings:odds");
        }
    }
}
