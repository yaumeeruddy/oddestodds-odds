﻿using Odds.CRUD;
using Odds.Utils;
using System;

namespace Odds.Screens
{
    public class OddsHandlerLoginScreen : HeaderScreen
    {
        public new void Draw()
        {
            base.Draw();
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("NOTE: For any input fields, enter the dot symbol (.) to go back to previous screen.");
            Console.ResetColor();
            var newLoginAttempt = true;
            Console.WriteLine("\nENTER YOUR LOGIN DETAILS");
            while (newLoginAttempt)
            {
                Console.WriteLine();
                string username = new ConsoleInput("Username", ConsoleInput.InputType.Text, true).ReadInput();

                if (username == ".")
                {
                    new MainMenuScreen().Draw();
                    return;
                }

                string password = new ConsoleInput("Password", ConsoleInput.InputType.Text, true, true).ReadInput();

                if (password == ".")
                {
                    new MainMenuScreen().Draw();
                    return;
                }

                if (OddsHandlerManagement.CheckLogin(username, password))
                {
                    new OddsHandlerMainScreen().Draw();
                    return;
                }
                else
                {
                    string tryNewLogin = new ConsoleInput(
                                "Incorrect username/password. Do you want to attempt login again? [Y/N] "
                                , ConsoleInput.InputType.YesNo
                                , false).ReadInput();
                    if(tryNewLogin != "Y")
                    {
                        newLoginAttempt = false;
                    }
                }
            }
            new MainMenuScreen().Draw();
        }
    }
}
