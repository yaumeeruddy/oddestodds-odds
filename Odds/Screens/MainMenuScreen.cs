﻿using Odds.CRUD;
using Odds.Entities;
using Odds.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TableDependency.SqlClient;
using TableDependency.SqlClient.Base.Enums;
using TableDependency.SqlClient.Base.EventArgs;

namespace Odds.Screens
{
    public class MainMenuScreen : HeaderScreen
    {
        public Thread Thread { get; set; }
        private readonly SqlTableDependency<Odd> _dependency;

        public int SelectedIndex { get; set; } = 0;
        private List<string> listMenu = new List<string>
        {
            "Login as Odds Handler",
            "Exit"
        };

        private List<Odd> ListPublishedOdds;

        public MainMenuScreen()
        {
            _dependency = new SqlTableDependency<Odd>(ConfigurationManager.GetConnectionString()
                                                        , "Odds"
                                                        , null);
            _dependency.OnChanged += _dependency_OnChanged;
        }

        public new void Draw()
        {
            ThreadStart threadStart = new ThreadStart(Redraw);
            Thread = new Thread(threadStart);
            Thread.Start();
        }

        private void Redraw()
        {
            while (true)
            {
                base.Draw();
                ConsoleOutput.ShowPublishedColumn = false;
                ConsoleOutput.DisplayListOddsHeader();
                DisplayOdds();
                _dependency.Start();
                Console.WriteLine();
                DisplayMenu();
                if (ReadEnterSelection())
                {
                    break;
                }
            }
            DoActionBasedOnSelectedIndex();
        }

        private void _dependency_OnChanged(object sender, RecordChangedEventArgs<Odd> e)
        {
            if (ListPublishedOdds != null)
            {
                if (e.ChangeType != ChangeType.None)
                {
                    switch (e.ChangeType)
                    {
                        case ChangeType.Delete:
                            ListPublishedOdds.Remove(ListPublishedOdds.FirstOrDefault(c => c.Id == e.Entity.Id));
                            break;
                        case ChangeType.Insert:
                            ListPublishedOdds.Add(e.Entity);
                            break;
                        case ChangeType.Update:
                            var oddsIndex = ListPublishedOdds.IndexOf(ListPublishedOdds.FirstOrDefault(c => c.Id == e.Entity.Id));
                            if (oddsIndex >= 0) ListPublishedOdds[oddsIndex] = e.Entity;
                            break;
                    }
                    Draw();
                }
            }
        }

        private void DisplayOdds()
        {
            if(ListPublishedOdds == null)
                ListPublishedOdds = OddsManagement.GetPublishedOdds();
            foreach (var odds in ListPublishedOdds)
            {
                Console.WriteLine(odds.ToString());
            }
        }

        private void DisplayMenu()
        {
            for (int i = 0; i < listMenu.Count; i++)
            {
                if (SelectedIndex == i)
                {
                    Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.Black;
                }
                Console.WriteLine(listMenu[i]);
                Console.ResetColor();
            }
        }

        private bool ReadEnterSelection()
        {
            ConsoleKeyInfo cKey = Console.ReadKey();
            if (cKey.Key == ConsoleKey.Enter)
            {
                return true;
            }
            else if (cKey.Key == ConsoleKey.UpArrow)
            {
                SelectedIndex = (SelectedIndex == 0) ? listMenu.Count - 1 : SelectedIndex - 1;
            }
            else if (cKey.Key == ConsoleKey.DownArrow)
            {
                SelectedIndex = (SelectedIndex == listMenu.Count - 1) ? 0 : SelectedIndex + 1;
            }
            return false;
        }

        private void DoActionBasedOnSelectedIndex()
        {
            if (SelectedIndex == 0)
            {
                new OddsHandlerLoginScreen().Draw();
            }
            else
            {
                Environment.Exit(0);
            }
        }
    }
}
