﻿using Odds.CRUD;
using Odds.Entities;
using Odds.Utils;
using System;
using System.Collections.Generic;

namespace Odds.Screens
{
    public class ViewPublishedOddsScreen : HeaderScreen
    {
        public new void Draw()
        {
            base.Draw();
            ConsoleOutput.ShowPublishedColumn = false;
            ConsoleOutput.DisplayListOddsHeader();
            DisplayPublishedOdds();
            Console.Write("\nPress any key to go back to previous screen...");
            Console.CursorVisible = true;
            Console.ReadKey();
            new OddsHandlerMainScreen().Draw();
        }

        private void DisplayPublishedOdds()
        {
            List<Odd> listOdds = OddsManagement.GetPublishedOdds();
            foreach (var odds in listOdds)
            {
                Console.WriteLine(odds.ToString());
            }
        }
    }
}
