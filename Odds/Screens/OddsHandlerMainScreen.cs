﻿using Odds.CRUD;
using Odds.Entities;
using Odds.Utils;
using System;
using System.Collections.Generic;

namespace Odds.Screens
{
    public class OddsHandlerMainScreen : HeaderScreen
    {
        private enum MenuAction
        {
            CreateOdds = 0,
            UpdateOdds = 1,
            DeleteOdds = 2,
            ViewPublishedOdds = 3,
            Exit = 4
        };

        private enum Screen
        {
            None = 0,
            CreateOddsScreen = 1,
            UpdateOddsScreen = 2,
            ViewPublishedOddsScreen = 3,
        };

        private bool MenuSelectionMode { get; set; } = true;
        private int SelectedMenuIndex { get; set; } = 0;
        private MenuAction SelectedMenuAction { get; set; } = MenuAction.CreateOdds;
        private Screen ShowNextScreen { get; set; } = Screen.None;
        private int SelectedOddsIndex { get; set; } = 0;
        private List<Odd> ListOdds { get; set; }

        private List<string> ListMenu = new List<string>
        {
            "Create odds",
            "Update odds",
            "Delete odds",
            "View published odds",
            "Exit"
        };

        public new void Draw()
        {
            while (true)
            {
                base.Draw();
                ConsoleOutput.ShowPublishedColumn = true;
                ConsoleOutput.DisplayListOddsHeader();
                DisplayOdds();
                Console.WriteLine();
                DisplayMenu();
                if (MenuSelectionMode && ReadEnterSelection())
                {
                    DoActionBasedOnSelectedMenuAction();
                }
                else if (!MenuSelectionMode)
                {
                    DoActionBasedOnSelectedOdds();
                }
                if (ShowNextScreen != Screen.None) break;
            }
            switch(ShowNextScreen)
            {
                case Screen.CreateOddsScreen:
                    new CreateOddsScreen().Draw();
                    return;
                case Screen.UpdateOddsScreen:
                    Odd odd = ListOdds[SelectedOddsIndex];
                    new UpdateOddsScreen(odd).Draw();
                    return;
                case Screen.ViewPublishedOddsScreen:
                    new ViewPublishedOddsScreen().Draw();
                    return;
                default:
                    break;
            }
        }

        private void DisplayOdds()
        {
            ListOdds = OddsManagement.GetAllOdds();
            var index = 0;
            foreach (var odds in ListOdds)
            {
                if (!MenuSelectionMode && SelectedOddsIndex == index)
                {
                    Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.Black;
                }
                Console.WriteLine(odds.ToString());
                Console.ResetColor();
                index++;
            }
        }

        private void DisplayMenu()
        {
            for (int i = 0; i < ListMenu.Count; i++)
            {
                if (SelectedMenuIndex == i)
                {
                    Console.BackgroundColor = MenuSelectionMode ? ConsoleColor.Gray : ConsoleColor.DarkGray;
                    Console.ForegroundColor = ConsoleColor.Black;
                }
                Console.WriteLine(ListMenu[i]);
                Console.ResetColor();
            }
        }

        private bool ReadEnterSelection()
        {
            ConsoleKeyInfo cKey = Console.ReadKey();
            if (cKey.Key == ConsoleKey.Enter)
            {
                return true;
            }
            else if (cKey.Key == ConsoleKey.UpArrow)
            {
                if (MenuSelectionMode)
                {
                    SelectedMenuIndex = (SelectedMenuIndex == 0) ? ListMenu.Count - 1 : SelectedMenuIndex - 1;
                    SelectedMenuAction = (MenuAction)SelectedMenuIndex;
                }
                else
                    SelectedOddsIndex = (SelectedOddsIndex == 0) ? ListOdds.Count - 1 : SelectedOddsIndex - 1;
            }
            else if (cKey.Key == ConsoleKey.DownArrow)
            {
                if (MenuSelectionMode)
                {
                    SelectedMenuIndex = (SelectedMenuIndex == ListMenu.Count - 1) ? 0 : SelectedMenuIndex + 1;
                    SelectedMenuAction = (MenuAction)SelectedMenuIndex;
                }
                else
                    SelectedOddsIndex = (SelectedOddsIndex == ListOdds.Count - 1) ? 0 : SelectedOddsIndex + 1;
            }
            else if (cKey.Key == ConsoleKey.Escape)
            {
                MenuSelectionMode = true;
            }
            return false;
        }

        private void DoActionBasedOnSelectedMenuAction()
        {
            switch (SelectedMenuAction)
            {
                case MenuAction.CreateOdds:
                    ShowNextScreen = Screen.CreateOddsScreen;
                    break;
                case MenuAction.UpdateOdds:
                    MenuSelectionMode = false;
                    break;
                case MenuAction.DeleteOdds:
                    MenuSelectionMode = false;
                    break;
                case MenuAction.ViewPublishedOdds:
                    ShowNextScreen = Screen.ViewPublishedOddsScreen;
                    break;
                case MenuAction.Exit:
                    Environment.Exit(0);
                    break;
                default:
                    break;
            }
        }

        private void DoActionBasedOnSelectedOdds()
        {
            switch (SelectedMenuAction)
            {
                case MenuAction.UpdateOdds:
                    if (ReadEnterSelection())
                    {
                        MenuSelectionMode = true;
                        ShowNextScreen = Screen.UpdateOddsScreen;
                    }
                    break;
                case MenuAction.DeleteOdds:
                    if (ReadEnterSelection())
                    {
                        string confirmDelete = new ConsoleInput(
                                                "Are you want to delete the selected odds? [Y/N] ",
                                                ConsoleInput.InputType.YesNo,
                                                true).ReadInput();
                        if (confirmDelete == "Y")
                        {
                            Odd odd = ListOdds[SelectedOddsIndex];
                            OddsManagement.DeleteOdd(odd.Id);
                            SelectedOddsIndex = 0;
                        }
                        MenuSelectionMode = true;
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
