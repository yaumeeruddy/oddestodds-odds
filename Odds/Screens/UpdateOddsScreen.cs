﻿using Odds.CRUD;
using Odds.Entities;
using Odds.Utils;
using System;
using System.Collections.Generic;

namespace Odds.Screens
{
    public class UpdateOddsScreen : HeaderScreen
    {
        private enum MenuAction
        {
            Name = 0,
            Description = 1,
            N1 = 2,
            NX = 3,
            N2 = 4,
            Published = 5,
            EndDate = 6,
            SaveChanges = 7,
            Back = 8,
            Exit = 9
        };

        private int SelectedMenuIndex { get; set; } = 0;
        private MenuAction SelectedMenuAction { get; set; } = MenuAction.Name;
        private bool BlnBack { get; set; } = false;

        private List<string> ListMenu = new List<string>
        {
            "Name",
            "Description",
            "Odds 1",
            "Odds X",
            "Odds 2",
            "Published",
            "End date",
            "Save Changes & Go back",
            "Back without saving changes",
            "Exit"
        };

        private Odd Odd { get; set; }

        public UpdateOddsScreen(Odd Odd)
        {
            this.Odd = Odd;
        }

        public new void Draw()
        {
            while (true)
            {
                base.Draw();
                ConsoleOutput.ShowPublishedColumn = true;
                Console.WriteLine("UPDATING ODDS\n");
                ConsoleOutput.DisplayListOddsHeader();
                Console.WriteLine(Odd.ToString());
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("\nNOTE: For any input fields, enter the dot symbol (.) to cancel it.\n");
                Console.ResetColor();
                DisplayMenu();
                if (ReadEnterSelection())
                {
                    DoActionBasedOnSelectedMenuAction();
                }
                if(BlnBack)
                {
                    new OddsHandlerMainScreen().Draw();
                    return;
                }
            }
        }

        private void DisplayMenu()
        {
            for (int i = 0; i < ListMenu.Count; i++)
            {
                if (SelectedMenuIndex == i)
                {
                    Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.Black;
                }
                Console.WriteLine(ListMenu[i]);
                Console.ResetColor();
            }
        }

        private bool ReadEnterSelection()
        {
            ConsoleKeyInfo cKey = Console.ReadKey();
            if (cKey.Key == ConsoleKey.Enter)
            {
                return true;
            }
            else if (cKey.Key == ConsoleKey.UpArrow)
            {
                SelectedMenuIndex = (SelectedMenuIndex == 0) ? ListMenu.Count - 1 : SelectedMenuIndex - 1;
                SelectedMenuAction = (MenuAction)SelectedMenuIndex;
            }
            else if (cKey.Key == ConsoleKey.DownArrow)
            {
                SelectedMenuIndex = (SelectedMenuIndex == ListMenu.Count - 1) ? 0 : SelectedMenuIndex + 1;
                SelectedMenuAction = (MenuAction)SelectedMenuIndex;
            }
            return false;
        }

        private void DoActionBasedOnSelectedMenuAction()
        {
            string currentValue = "unassigned";
            switch (SelectedMenuAction)
            {
                case MenuAction.Name:
                    currentValue = Odd.Name;
                    Console.WriteLine($"\nCurrent value is {currentValue}. Enter new value.");
                    string name = new ConsoleInput("Name", ConsoleInput.InputType.Text, true).ReadInput();
                    if(name != ".")
                        Odd.Name = name;
                    break;
                case MenuAction.Description:
                    if (Odd.Description != null) currentValue = Odd.Description;
                    Console.WriteLine($"\nCurrent value is {currentValue}. Enter new value.");
                    string description = new ConsoleInput("Description", ConsoleInput.InputType.Text, false).ReadInput();
                    if (description != ".")
                        Odd.Description = description;
                    break;
                case MenuAction.N1:
                    currentValue = Odd.N1.ToString();
                    Console.WriteLine($"\nCurrent value is {currentValue}. Enter new value.");
                    string strN1 = new ConsoleInput("Odds 1", ConsoleInput.InputType.Decimal, true).ReadInput();
                    decimal n1 = 0;
                    if (strN1 != "." && decimal.TryParse(strN1, out n1))
                    {
                        Odd.N1 = n1;
                    }
                    break;
                case MenuAction.NX:
                    currentValue = Odd.NX.ToString();
                    Console.WriteLine($"\nCurrent value is {currentValue}. Enter new value.");
                    string strNX = new ConsoleInput("Odds X", ConsoleInput.InputType.Decimal, true).ReadInput();
                    decimal nX = 0;
                    if (strNX != "." && decimal.TryParse(strNX, out nX))
                    {
                        Odd.NX = nX;
                    }
                    break;
                case MenuAction.N2:
                    currentValue = Odd.N2.ToString();
                    Console.WriteLine($"\nCurrent value is {currentValue}. Enter new value.");
                    string strN2 = new ConsoleInput("Odds 2", ConsoleInput.InputType.Decimal, true).ReadInput();
                    decimal n2 = 0;
                    if (strN2 != "." && decimal.TryParse(strN2, out n2))
                    {
                        Odd.N2 = n2;
                    }
                    break;
                case MenuAction.Published:
                    string publishedOrNot = Odd.Published ? "" : "not";
                    string message = $"\nOdds is currently {publishedOrNot} published. Enter Y for opposite changes / N for no changes.";
                    string strPublished = new ConsoleInput(message, ConsoleInput.InputType.YesNo, true).ReadInput();
                    if(strPublished == "Y")
                    {
                        Odd.Published = !Odd.Published;
                    }
                    break;
                case MenuAction.EndDate:
                    if (Odd.EndDate != null) currentValue = ((DateTime)Odd.EndDate).ToString("dd/MM/yyyy");
                    Console.WriteLine($"\nCurrent value is {currentValue}. Enter new value.");
                    string strEndDate = new ConsoleInput("End date", ConsoleInput.InputType.Date, false).ReadInput();
                    DateTime endDate = DateTime.Now;
                    if(string.IsNullOrEmpty(strEndDate))
                    {
                        Odd.EndDate = null;
                    }
                    else if (strEndDate != "." && DateTime.TryParse(strEndDate, out endDate))
                    {
                        Odd.EndDate = endDate;
                    }
                    break;
                case MenuAction.SaveChanges:
                    OddsManagement.UpdateOdd(Odd);
                    BlnBack = true;
                    break;
                case MenuAction.Back:
                    BlnBack = true;
                    break;
                case MenuAction.Exit:
                    Environment.Exit(0);
                    break;
                default:
                    break;
            }
        }
    }
}
