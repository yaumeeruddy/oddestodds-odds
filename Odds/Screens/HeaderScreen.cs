﻿using Odds.Interfaces;
using System;

namespace Odds.Screens
{
    public class HeaderScreen : IScreen
    {
        public void Draw()
        {
            Console.CursorVisible = false;
            Console.Clear();
            Console.BackgroundColor = ConsoleColor.Cyan;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("ODDESTODDS.com");
            Console.WriteLine();
            Console.ResetColor();
        }
    }
}
