﻿using Odds.CRUD;
using Odds.Entities;
using Odds.Utils;
using System;
using System.Collections.Generic;

namespace Odds.Screens
{
    public class CreateOddsScreen : HeaderScreen
    {
        private List<ConsoleInput> ListInputs = new List<ConsoleInput>();

        public bool AllInputsDone  { get; set; } = true;

        public new void Draw()
        {
            base.Draw();
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("NOTE: For any input fields, enter the dot symbol (.) to go back to previous screen.");
            Console.ResetColor();
            Console.WriteLine("\nENTER ODDS DETAILS\n");

            string name = null;
            string description = null;
            decimal n1 = 0;
            decimal nX = 0;
            decimal n2 = 0;
            bool published = false;
            DateTime? endDate = null;

            ListInputs.Add(new ConsoleInput("Name", ConsoleInput.InputType.Text, true));
            ListInputs.Add(new ConsoleInput("Description", ConsoleInput.InputType.Text, false));
            ListInputs.Add(new ConsoleInput("Odds 1", ConsoleInput.InputType.Decimal, true));
            ListInputs.Add(new ConsoleInput("Odds X", ConsoleInput.InputType.Decimal, true));
            ListInputs.Add(new ConsoleInput("Odds 2", ConsoleInput.InputType.Decimal, true));
            ListInputs.Add(new ConsoleInput("Publish odds (Y/N)? ", ConsoleInput.InputType.YesNo, true));
            ListInputs.Add(new ConsoleInput("End date", ConsoleInput.InputType.Date, true));

            for (int i = 0; i < ListInputs.Count; i++)
            {
                string inputValue = null;
                bool blnContinueInputOnDotEntry;
                do
                {
                    blnContinueInputOnDotEntry = false;
                    inputValue = ListInputs[i].ReadInput();
                    if (!string.IsNullOrEmpty(inputValue) && inputValue == ".")
                    {
                        string cancelCreateOdds = new ConsoleInput("Entered dot symbol (.) , go back to previous screen (Y/N)? "
                                                                , ConsoleInput.InputType.YesNo
                                                                , true).ReadInput();
                        Console.WriteLine();
                        if(cancelCreateOdds == "Y")
                        {
                            AllInputsDone = false;
                        }
                        else
                        {
                            blnContinueInputOnDotEntry = true;
                        }
                    }
                } while (blnContinueInputOnDotEntry);

                if (!AllInputsDone) break;

                switch (i)
                {
                    case 0:
                        name = inputValue;
                        break;
                    case 1:
                        description = inputValue;
                        break;
                    case 2:
                        decimal.TryParse(inputValue, out n1);
                        break;
                    case 3:
                        decimal.TryParse(inputValue, out nX);
                        break;
                    case 4:
                        decimal.TryParse(inputValue, out n2);
                        break;
                    case 5:
                        published = inputValue == "Y";
                        break;
                    case 6:
                        if (!string.IsNullOrEmpty(inputValue))
                        {
                            DateTime date;
                            DateTime.TryParse(inputValue, out date);
                            endDate = date;
                        }
                        break;
                    default:
                        break;
                }
            }
            if(AllInputsDone)
            {
                string saveChanges = new ConsoleInput("Save changes (Y/N)? ", ConsoleInput.InputType.YesNo, true).ReadInput();
                if (saveChanges == "Y")
                {
                    Odd odd = new Odd
                    {
                        Name = name,
                        Description = description,
                        N1 = n1,
                        N2 = n2,
                        NX = nX,
                        OddsHandlerId = OddsHandlerManagement.ConnectedOddsHandler.Id,
                        Published = published,
                        EndDate = endDate
                    };
                    OddsManagement.AddOdd(odd);
                }
            }
            new OddsHandlerMainScreen().Draw();
        }
    }
}
