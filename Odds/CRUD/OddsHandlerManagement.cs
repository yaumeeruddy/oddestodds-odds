﻿using Odds.Entities;
using System.Linq;

namespace Odds.CRUD
{
    public class OddsHandlerManagement
    {
        public static OddsHandler ConnectedOddsHandler;

        public static void AddDummyUserIfEmpty()
        {
            var _context = ServiceManager.GetDbContext();
            var count = _context.OddsHandlers.Count();
            if(count == 0)
            {
                var username = ConfigurationManager.GetValue("DummyOddsHandler:Username");
                var password = ConfigurationManager.GetValue("DummyOddsHandler:Password");
                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {
                    OddsHandler oh = new OddsHandler
                    {
                        Username = username,
                        Password = password
                    };
                    _context.OddsHandlers.Add(oh);
                    _context.SaveChanges();
                }
            }
        }

        public static bool CheckLogin(string username, string password)
        {
            var _context = ServiceManager.GetDbContext();
            ConnectedOddsHandler = _context.OddsHandlers
                                            .Where(x => x.Username == username && x.Password == password)
                                            .SingleOrDefault();
            return ConnectedOddsHandler != null;
        }
    }
}
