﻿using Odds.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Odds.CRUD
{
    public class OddsManagement
    {
        public static void AddOdd(Odd odd)
        {
            var _context = ServiceManager.GetDbContext();
            _context.Odds.Add(odd);
            _context.SaveChanges();
        }

        public static void UpdateOdd(Odd odd)
        {
            var _context = ServiceManager.GetDbContext();
            var dbOdd = _context.Odds
                               .Where(x => x.Id == odd.Id)
                               .SingleOrDefault();
            if(dbOdd != null)
            {
                odd.LastUpdateDate = DateTime.Now;
                odd.OddsHandlerId = OddsHandlerManagement.ConnectedOddsHandler.Id;
                _context.Odds.Update(odd);
                _context.SaveChanges();
            }
        }

        public static void DeleteOdd(int oddId)
        {
            var _context = ServiceManager.GetDbContext();
            var odd = _context.Odds
                               .Where(x => x.Id == oddId)
                               .SingleOrDefault();
            _context.Odds.Remove(odd);
            _context.SaveChanges();
        }

        public static List<Odd> GetAllOdds()
        {
            var _context = ServiceManager.GetDbContext();
            var odds = _context.Odds.ToList();
            return odds;
        }

        public static List<Odd> GetPublishedOdds()
        {
            var _context = ServiceManager.GetDbContext();
            var today = DateTime.Now;
            var odds = _context.Odds
                               .Where(x => x.Published == true
                                            && (
                                                x.EndDate == null
                                                || ((DateTime)x.EndDate).Date.AddDays(1) > today))
                               .ToList();
            return odds;
        }
    }
}
