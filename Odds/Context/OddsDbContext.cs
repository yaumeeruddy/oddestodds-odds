﻿using Microsoft.EntityFrameworkCore;
using Odds.Entities;
using Odds.Interfaces;

namespace Odds.Context
{
    public class OddsDbContext : DbContext, IOddsDbContext
    {
        public OddsDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            SetConfigurationForOddsHandler(modelBuilder);
            SetConfigurationForOdds(modelBuilder);
        }

        private void SetConfigurationForOddsHandler(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OddsHandler>()
                    .Property(s => s.Id)
                    .IsRequired();

            modelBuilder.Entity<OddsHandler>()
                    .Property(s => s.Username)
                    .HasMaxLength(50)
                    .IsRequired();

            modelBuilder.Entity<OddsHandler>()
                    .Property(s => s.Password)
                    .HasMaxLength(25)
                    .IsRequired();
        }

        private void SetConfigurationForOdds(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Odd>()
                    .Property(s => s.Id)
                    .IsRequired();

            modelBuilder.Entity<Odd>()
                    .Property(s => s.Name)
                    .HasMaxLength(100)
                    .IsRequired();

            modelBuilder.Entity<Odd>()
                    .Property(s => s.Description)
                    .HasMaxLength(255);

            modelBuilder.Entity<Odd>()
                    .Property(s => s.Published)
                    .HasDefaultValue(false);

            modelBuilder.Entity<Odd>()
                    .Property(s => s.CreatedDate)
                    .HasDefaultValueSql("GETDATE()");

            modelBuilder.Entity<Odd>()
                    .Property(s => s.LastUpdateDate)
                    .HasDefaultValueSql("GETDATE()");

            modelBuilder.Entity<Odd>()
                        .HasOne(x => x.OddsHandler)
                        .WithMany(y => y.Odds)
                        .HasForeignKey(x => x.OddsHandlerId);
        }

        public DbSet<OddsHandler> OddsHandlers { get; set; }
        public DbSet<Odd> Odds { get; set; }
    }
}
