﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Odds.Context
{
    public class OddsDbContextFactory : IDesignTimeDbContextFactory<OddsDbContext>
    {
        public OddsDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<OddsDbContext>();
            optionsBuilder.UseSqlServer(ConfigurationManager.GetConnectionString());
            return new OddsDbContext(optionsBuilder.Options);
        }
    }
}