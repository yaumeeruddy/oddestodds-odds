﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Odds.Context;
using Odds.Interfaces;

namespace Odds
{
    public class ServiceManager
    {
        public static ServiceProvider ServiceProvider { get; private set; }

        private static IOddsDbContext _context { get; set; }

        public static void Init()
        {
            ServiceProvider = new ServiceCollection()
                                    .AddLogging()
                                    .AddDbContext<IOddsDbContext, OddsDbContext>(options =>
                                        options.UseSqlServer(ConfigurationManager.GetConnectionString()))
                                    .BuildServiceProvider();
        }

        public static IOddsDbContext GetDbContext()
        {
            if(_context == null)
                _context = ServiceProvider.GetService<IOddsDbContext>();
            return _context;
        }
    }
}
