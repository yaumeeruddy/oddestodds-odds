﻿using System;

namespace Odds.Utils
{
    public class ConsoleInput
    {
        public enum InputType
        {
            Text = 0,
            Decimal = 1,
            Date = 2,
            YesNo = 3
        }

        public string Label { get; set; }
        public InputType InputTypeProp { get; set; }
        public bool IsPasswordInput { get; set; }
        public bool Required { get; set; }

        public ConsoleInput(string Label, InputType InputType, bool Required, bool IsPasswordInput = false)
        {
            this.Label = Label;
            InputTypeProp = InputType;
            this.IsPasswordInput = IsPasswordInput;
            this.Required = Required;
        }

        public string ReadInput()
        {
            Console.CursorVisible = true;
            switch (InputTypeProp)
            {
                case InputType.Text:
                    return ReadInputText();
                case InputType.Decimal:
                    return ReadInputDecimal();
                case InputType.Date:
                    return ReadInputDate();
                case InputType.YesNo:
                    return ReadYNKey();
                default:
                    break;
            }
            return null;
        }

        private string ReadInputText()
        {
            var labelOutput = $"{Label} : ";
            string value = null;
            var valid = true;
            do
            {
                valid = true;
                value = IsPasswordInput ? ReadLine.ReadPassword(labelOutput) : ReadLine.Read(labelOutput);
                if (Required && string.IsNullOrEmpty(value))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"{Label} is required.");
                    Console.ResetColor();
                    valid = false;
                }
                if (value == "") value = null;
            } while (!valid);
            Console.CursorVisible = false;
            return value;
        }

        private string ReadInputDate()
        {
            var labelOutput = $"{Label} : ";
            string value = null;
            var valid = true;
            DateTime date;
            do
            {
                valid = true;
                value = ReadLine.Read(labelOutput);
                if (!string.IsNullOrEmpty(value) && value != "." && !DateTime.TryParse(value, out date))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Please enter a date, with correct format (E.g. dd/MM/yyyy)");
                    Console.ResetColor();
                    valid = false;
                }
            } while (!valid);
            Console.CursorVisible = false;
            return value;
        }

        private string ReadInputDecimal()
        {
            var labelOutput = $"{Label} : ";
            string value = null;
            var valid = true;
            decimal dec;

            do
            {
                valid = true;
                value = ReadLine.Read(labelOutput);
                if (string.IsNullOrEmpty(value) || (value != "." && !decimal.TryParse(value, out dec)))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Please enter a decimal value.");
                    Console.ResetColor();
                    valid = false;
                }
            } while (!valid);
            Console.CursorVisible = false;
            return value;
        }

        private string ReadYNKey()
        {
            ConsoleKeyInfo cKeyInfo;
            bool continueLoop = true;
            do
            {
                Console.WriteLine();
                Console.Write(Label);
                cKeyInfo = Console.ReadKey(true);
                continueLoop = (cKeyInfo.KeyChar != '.'
                    && cKeyInfo.Key != ConsoleKey.Y
                    && cKeyInfo.Key != ConsoleKey.N);
            } while (continueLoop);
            Console.CursorVisible = false;
            Console.WriteLine();
            if (cKeyInfo.KeyChar == '.')
            {
                return ".";
            }
            else if (cKeyInfo.Key == ConsoleKey.Y)
            {
                return "Y";
            }
            return "N";
        }
    }
}