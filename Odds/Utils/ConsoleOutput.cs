﻿using System;

namespace Odds.Utils
{
    public class ConsoleOutput
    {
        public static bool ShowPublishedColumn { get; set; } = false;

        public static void DisplayListOddsHeader()
        {
            string format = "| {0,35} | {1,25} | {2,10} | {3,10} | {4,10} | {5,10} |";
            if (ShowPublishedColumn) format += " {6,10} |";
            var output = string.Format(format
                                        , StringUtils.PadBoth("Name",35)
                                        , StringUtils.PadBoth("Description", 25)
                                        , StringUtils.PadBoth("1",10)
                                        , StringUtils.PadBoth("X",10)
                                        , StringUtils.PadBoth("2",10)
                                        , StringUtils.PadBoth("End date",10)
                                        , StringUtils.PadBoth("Published", 10));
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(output);
            Console.ResetColor();
        }        
    }
}
