﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Odds.Entities
{
    public class OddsHandler
    {
        public int Id { get; set; }
        public string Username { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        public ICollection<Odd> Odds { get; set; }
    }
}
