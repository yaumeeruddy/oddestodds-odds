﻿using Odds.Utils;
using System;

namespace Odds.Entities
{
    public class Odd
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal N1 { get; set; }
        public decimal N2 { get; set; }
        public decimal NX { get; set; }
        public int OddsHandlerId { get; set; }
        public bool Published { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public DateTime? EndDate { get; set; }
        public OddsHandler OddsHandler { get; set; }

        public new string ToString()
        {
            string strDescription = Description == null ? "" : Description;
            string published = Published ? "YES" : "NO";
            string strEndDate = "(NONE)";
            if(EndDate != null) { strEndDate = ((DateTime)EndDate).ToString("dd/MM/yyyy"); }
            string format = "| {0,35} | {1,25} | {2,10} | {3,10} | {4,10} | {5,10} |";
            if (ConsoleOutput.ShowPublishedColumn) format += " {6,10} |";
            return string.Format(format
                                    , Name.PadRight(35)
                                    , strDescription.PadRight(25)
                                    , StringUtils.PadBoth(N1.ToString(), 10)
                                    , StringUtils.PadBoth(NX.ToString(), 10)
                                    , StringUtils.PadBoth(N2.ToString(), 10)
                                    , StringUtils.PadBoth(strEndDate, 10)
                                    , StringUtils.PadBoth(published, 10));
        }
    }
}
